import json
from functools import wraps

from flask import request, jsonify, make_response
from sqlalchemy.orm import joinedload, raiseload, lazyload

from app.models.employee_details import EmployeeDetails
from app.models.employee_address import EmployeeAddress
from app.models.person_details import PersonDetails
from app import db, app
import jwt
import datetime
from werkzeug.security import generate_password_hash, check_password_hash


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'access-tokens' in request.headers:
            token = request.headers['access-tokens']
        if not token:
            return jsonify({'message': 'A valid token is missing'})
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_employee = EmployeeDetails.query.filter_by(id=data['id']).first()
        except:
            return jsonify({'message': 'token is invalid'})

        return f(current_employee, *args, **kwargs)

    return decorator


def emp_post(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    # id = request.json.get('id')
    request_data = json.loads(request.form.get('data'))
    req_data : dict = request_data['data']
    admin = req_data['admin']
    emp_username = req_data['emp_username']
    # data = request.get_json()
    hashed_password = generate_password_hash(req_data['emp_password'], method='sha256')
    emp_password = hashed_password
    job_role = req_data['job_role']
    experience = req_data['experience']
    salary = req_data['salary']
    employee_details = EmployeeDetails()
    employee_details.admin = admin
    employee_details.emp_username = emp_username
    employee_details.emp_password = emp_password
    employee_details.job_role = job_role
    employee_details.experience = experience
    employee_details.salary = salary
    db.session.add(employee_details)
    db.session.commit()
    return jsonify({'message': 'New employee details was inserted'})


def emp_get_all(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    fetchEmployees = EmployeeDetails.query.all()
    result = []
    for employee in fetchEmployees:
        employees = {}
        employees['id'] = employee.id
        employees['admin'] = employee.admin
        employees['emp_username'] = employee.emp_username
        employees['emp_password'] = employee.emp_password
        employees['job_role'] = employee.job_role
        employees['experience'] = employee.experience
        employees['salary'] = employee.salary
        result.append(employees)
    return jsonify(result)


def emp_get(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    id = request.form.get('id')
    fetchUser = EmployeeDetails.query.filter(EmployeeDetails.id == id).all()
    response = []
    for users in fetchUser:
        user = users.__dict__
        user.pop('_sa_instance_state')

        response.append(user)
    return jsonify({'The employees you fetched': response})

def emp_update(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    request_body = json.loads(request.form.get('data'))
    req_body :dict = request_body['data']
    emp_username = req_body['emp_username']
    job_role = req_body['job_role']
    employee_details = EmployeeDetails.query.filter(EmployeeDetails.emp_username == emp_username).first()
    employee_details.emp_username = emp_username
    employee_details.job_role = job_role
    db.session.commit()
    return 'The changes you have made was updated'


def emp_delete(current_employee, id):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    emp_details_delete = EmployeeDetails.query.filter(EmployeeDetails.id == id).delete()
    print(emp_details_delete)
    db.session.commit()
    return 'The employee details you want to delete was deleted'


def person_post(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    request_body=json.loads(request.form.get('data'))
    req_body :dict = request_body['data']
    emp_id = req_body['emp_id']
    Name = req_body['Name']
    Age = req_body['Age']
    Gender = req_body['Gender']
    Marital_status = req_body['Marital_status']
    person_details = PersonDetails()
    person_details.emp_id = emp_id
    person_details.Name = Name
    person_details.Age = Age
    person_details.Gender = Gender
    person_details.Marital_status = Marital_status
    db.session.add(person_details)
    db.session.commit()
    return 'Person details was inserted'


def person_get_all(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    fetchpeople = PersonDetails.query.all()
    result1 = []
    for person in fetchpeople:
        people = {}
        people['person_id'] = person.person_id
        people['emp_id'] = person.emp_id
        people['Name'] = person.Name
        people['Age'] = person.Age
        people['Gender'] = person.Gender
        people['Marital_status'] = person.Marital_status

        result1.append(people)
    return jsonify(result1)


def person_update(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    Name = request.json.get('Name')
    Marital_status = request.json.get('Marital_status')
    person_details = PersonDetails.query.filter(PersonDetails.Name == Name).first()
    person_details.Name = Name
    person_details.Marital_status = Marital_status
    db.session.commit()
    return 'The changes you have made was updated'


def person_delete(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    person_id = request.json.get('person_id')
    person_details_delete = PersonDetails.query.filter(PersonDetails.person_id == person_id).delete()
    print(person_details_delete)
    db.session.commit()
    return 'The person details you want to delete was deleted'


def addr_post(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    employee_id = request.json.get('employee_id')
    Address = request.json.get('Address')
    City = request.json.get('City')
    Pincode = request.json.get('Pincode')
    employee_address = EmployeeAddress()
    employee_address.employee_id = employee_id
    employee_address.Address = Address
    employee_address.City = City
    employee_address.Pincode = Pincode
    db.session.add(employee_address)
    db.session.commit()
    return 'Employee addresses was inserted'


def emp_address_get_all(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    fetchaddress = PersonDetails.query.all()
    result2 = []
    for address in fetchaddress:
        addresses = {}
        addresses['address_id'] = address.address_id
        addresses['employee_id'] = address.employee_id
        addresses['Address'] = address.Address
        addresses['City'] = address.City
        addresses['Pincode'] = address.Pincode

        result2.append(addresses)
    return jsonify(result2)


def address_update(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    address_id = request.json.get('address_id')
    Address = request.json.get('Address')
    address_details = EmployeeAddress.query.filter(EmployeeAddress.address_id == address_id).first()
    address_details.address_id = address_id
    address_details.Address = Address
    db.session.commit()
    return 'The changes you have made was updated'


def address_delete(current_employee):
    if not current_employee.admin:
        return jsonify({'message': 'You do not have access to this page'})
    address_id = request.json.get('address_id')
    address_details_delete = EmployeeAddress.query.filter(EmployeeAddress.address_id == address_id).delete()
    print(address_details_delete)
    db.session.commit()
    return 'The address details you want to delete was deleted'

def merge_emp_address():
    eager_fetch=db.session.query(EmployeeDetails)\
        .options(joinedload(EmployeeDetails.emp_rel,innerjoin=True))\
        .filter_by(emp_username='Janet_google').all()
    print(eager_fetch)
    merge_response = []
    for response in eager_fetch:
        user = response.__dict__
        user.pop('_sa_instance_state')

        merge_response.append(user)
    print(merge_response)
    return jsonify({'message':"You successfully fetched eagerfetch requirements"})

def lazy_fetch():
    lazy_fetch = db.session.query(EmployeeDetails) \
        .options(lazyload(EmployeeDetails.emp_rel)).all()
    print(lazy_fetch)
    merge_response1 = []
    for responses in lazy_fetch:
        users = responses.__dict__
        users.pop('_sa_instance_state')

        merge_response1.append(users)
    print(merge_response1)
    return jsonify({'message': "You successfully fetched the lazyfetch requirements"})
    # print(lazy_fetch)

def login():
    auth = request.authorization
    if auth and auth.password == 'emp_password':
        return make_response("login required!!")
    user = EmployeeDetails.query.filter_by(emp_username=auth.username).first()
    if not user:
        return make_response(("login required!"))
    if check_password_hash(user.emp_password, auth.password):
        token = jwt.encode({'id': user.id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=120)},
                           app.config['SECRET_KEY'])
        return jsonify({'token': token.decode('UTF-8')})
