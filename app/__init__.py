from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from app.config import Database_URI
import pymysql

pymysql.install_as_MySQLdb()
app = Flask(__name__)
db = SQLAlchemy()


def create_app():
    app.config['SQLALCHEMY_DATABASE_URI'] = Database_URI
    app.config['SECRET_KEY'] = 'secret_key'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    from app.models.employee_details import EmployeeDetails
    from app.models.employee_address import EmployeeAddress
    from app.models.person_details import PersonDetails
    from app.api.routes import emp_post, addr_post, person_post, login
    db.init_app(app)
    with app.app_context():
        db.create_all()
    return app
