from app import db
from sqlalchemy import orm

class EmployeeAddress(db.Model):
    __tablename__ = 'employee_address'
    address_id = db.Column(db.Integer, primary_key=True,autoincrement = True)
    employee_id = db.Column (db.Integer,db.ForeignKey('employee_details.id'))
    Address= db.Column(db.String(100), index=False)
    City= db.Column(db.String(100), index=False)
    Pincode = db.Column(db.Integer)

    # address_rel = orm.relationship('EmployeeDetails', lazy="joined")
