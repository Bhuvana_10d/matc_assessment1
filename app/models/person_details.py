from app import db
from sqlalchemy import orm

class PersonDetails(db.Model):
    __tablename__ = 'person_details'
    person_id = db.Column(db.Integer, primary_key=True, autoincrement = True)
    emp_id = db.Column (db.Integer,db.ForeignKey('employee_details.id'))
    Name= db.Column(db.String(100), index=False)
    Age = db.Column(db.String(100), index=False)
    Gender = db.Column(db.String(100), index=False)
    Marital_status = db.Column(db.String(100),index=False)

    # person_rel= orm.relationship('EmployeeDetails',lazy = "joined")
