from app import db, app

class EmployeeDetails(db.Model):
    __tablename__ = 'employee_details'

    id = db.Column(db.Integer, primary_key=True,autoincrement = True)
    admin = db.Column(db.Boolean)
    emp_username = db.Column(db.String(36))
    emp_password = db.Column(db.String(150))
    job_role= db.Column(db.String(100), index=False)
    experience = db.Column(db.String(100), index=False)
    salary = db.Column(db.Integer)

    emp_rel = db.relationship('EmployeeAddress',lazy = "joined")
