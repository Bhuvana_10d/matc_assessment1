from flasgger import swag_from

from app.api.swagger_UI_specifications import post_specs,get_specs,put_specs,delete_specs
from app.services.db_logics import emp_post, person_post, addr_post, \
    login, emp_get_all, emp_get, token_required, emp_update, emp_delete, person_get_all, \
    person_update, person_delete, address_delete, address_update, emp_address_get_all, \
    merge_emp_address, lazy_fetch
from app import app

from flask import Blueprint

employees_details = Blueprint('employee_details', __name__)
people_details = Blueprint('people_details', __name__)
employee_address = Blueprint('employee_address', __name__)


class EmployeeDetails():

    @staticmethod
    @employees_details.route('/insert', methods=['POST'])
    @swag_from(post_specs, methods=['POST'])
    @token_required
    def employee_details(current_employee):
        return emp_post(current_employee)

    @staticmethod
    @employees_details.route('/get_all', methods=['GET'])
    @swag_from(get_specs, methods=['GET'])
    @token_required
    def get_all_employee(current_employee):
        return emp_get_all(current_employee)

    @staticmethod
    @employees_details.route('/update', methods=['PUT'])
    @swag_from(put_specs, methods=['PUT'])
    @token_required
    def update_emp_details(current_employee):
        return emp_update(current_employee)

    @staticmethod
    @employees_details.route('/delete/<int:id>', methods=['DELETE'])
    @swag_from(delete_specs, methods=['DELETE'])
    @token_required
    def delete_emp_details(current_employee, id):
        return emp_delete(current_employee, id=id)

    @staticmethod
    @app.route('/get', methods=['GET'])
    @token_required
    def get_emp_details(current_employee):
        return emp_get(current_employee)

    @staticmethod
    @people_details.route('/insert1', methods=['POST'])
    @swag_from(people_details,methods = ['POST'])
    @token_required
    def person_details(current_employee):
        return person_post(current_employee)

    @staticmethod
    @app.route('/get_all1', methods=['GET'])
    @token_required
    def get_all_person_details(current_employee):
        return person_get_all(current_employee)

    @staticmethod
    @app.route('/update1', methods=['PUT'])
    @token_required
    def update_person_details(current_employee):
        return person_update(current_employee)

    @staticmethod
    @app.route('/delete1', methods=['DELETE'])
    @token_required
    def delete_person_details(current_employee):
        return person_delete(current_employee)

    @staticmethod
    @app.route('/insert2', methods=['POST'])
    @token_required
    def employee_address(current_employee):
        return addr_post(current_employee)

    @staticmethod
    @app.route('/get_all2', methods=['GET'])
    @token_required
    def get_address(current_employee):
        return emp_address_get_all(current_employee)

    @staticmethod
    @app.route('/update2', methods=['UPDATE'])
    @token_required
    def update_address(current_employee):
        return address_update(current_employee)

    @staticmethod
    @app.route('/delete2', methods=['DELETE'])
    @token_required
    def delete_address(current_employee):
        return address_delete(current_employee)

    @staticmethod
    @app.route('/eager_fetch', methods=['GET'])
    def eager_fetching():
        return merge_emp_address()

    @staticmethod
    @app.route('/lazy_fetch', methods=['GET'])
    def lazy_fetching():
        return lazy_fetch()

    @staticmethod
    @app.route('/login')
    def emp_login():
        return login()
