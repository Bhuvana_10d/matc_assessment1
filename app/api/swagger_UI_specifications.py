post_specs = {
    "security": [
        {
            "AdminAuthToken": []
        }
    ],
    "requestBody": {
        "content": {
            "multipart/form-data": {
                "schema": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "required": [
                                        "admin",
                                        "emp_username",
                                        "emp_password",
                                        "job_role",
                                        "experience",
                                        "salary"
                                    ],
                                    "properties": {
                                        "admin": {
                                            "type": "boolean"
                                        },
                                        "emp_username": {
                                            "type": "string"
                                        },
                                        "emp_password": {
                                            "type": "string"
                                        },
                                        "job_role": {
                                            "type": "string"
                                        },
                                        "experience": {
                                            "type": "string"
                                        },
                                        "salary": {
                                            "type": "integer"
                                        },
                                    },
                                    "type": "object"
                                }
                            }
                        },
                    },
                    "required": [
                        "data"
                    ],
                }
            },
        },
    },
    "responses": {
        "200": {
            "description": "Success",
            "content": {
                "application/json": {
                    "example": {
                    }
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "SEND_VALUE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title": "Error while Inserting the employee "
                                         "details "
                            }
                        ]
                    }
                }
            }
        }
    },

}
get_specs = {
    "security": [
        {
            "AdminAuthToken": []
        }
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/pdf": {
                    "example": "The data you fetched"
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "GET_VALUES.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while getting values from db "
                            }
                        ]
                    }
                }
            }
        }
    },
}
put_specs = {
    "security": [
        {
            "AdminAuthToken": []
        }
    ],
    "parameters": [
        {
            "name": "data",
            "in": "formData",
            "description": "request body",
            "required": True,
            "type": "string",
            "default": {
                "data": {
                    'types': 'Employee data ',
                    'admin': 'boolean',
                    'emp_username': 'string',
                    'emp_password': 'string',
                    'job_role': 'string',
                    'experience': 'string',
                    'salary': 'string'
                }
            },
        }

    ],
    "requestBody": {
        "content": {
            "multipart/form-data": {
                "schema": {
                    "type": "object",
                    "properties": {
                        "data": {
                            "type": "object",
                            "properties": {
                                "data": {
                                    "required": [
                                        "emp_username",
                                        "job_role"
                                    ],
                                    "properties": {
                                        "emp_username": {
                                            "type": "string"
                                        },
                                        "job_role": {
                                            "type": "string"
                                        },
                                    },
                                    "type": "object"
                                }
                            }
                        },
                    },
                    "required": [
                        "data"
                    ],
                }
            },
        },
    },
    "responses": {
        "200": {
            "description": "Success",
            "content": {
                "application/json": {
                    "example": {
                    }
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "SEND_VALUE.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title": "Error while Inserting the employee "
                                         "details "
                            }
                        ]
                    }
                }
            }
        }
    },
}

delete_specs = {
    "security": [
        {
            "AdminAuthToken": []
        }
    ],
    "parameters": [
        {
            "name": "id",
            "in": "path",
            "required": True,
            "schema": {
                "type": "string"
            }
        },
    ],
    "responses": {
        "200": {
            "description": "success",
            "content": {
                "application/pdf": {
                    "example": "The data you want to delete was deleted"
                }
            }
        },
        "400": {
            "description": "Validation error",
            "content": {
                "application/json": {
                    "example": {
                        "errors": [
                            {
                                "code": "delete_VALUES.ERROR",
                                "detail": {
                                    "error": "string"
                                },
                                "status": 400,
                                "title":
                                    "Error while delete values from db "
                            }
                        ]
                    }
                }
            }
        }
    },
}
