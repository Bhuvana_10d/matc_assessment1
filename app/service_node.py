from flasgger import Swagger
from app.api.routes import employees_details,people_details,employee_address

from app import app

def register_blueprint():
    print('Register Blueprints')
    app.register_blueprint(employees_details, url_prefix='/app/emp_details')
    app.register_blueprint(people_details, url_prefix='/app/people_details')
    app.register_blueprint(employee_address, url_prefix='/app/emp_address')
    swagger_template = {
        'components': {
            "securitySchemes": {
                "AdminAuthToken": {
                    "type": "apiKey",
                    "in": "header",
                    "name": "Authorization"
                }
            }
        }
    }

    swagger_config = {"headers": [], 'title': 'swagger_api_documentation',
                      "openapi": "3.0.3", "specs": [
            {"endpoint": 'app', "route": '/apidocs'}],
                      "swagger_ui": True, "specs_route": "/app/apidocs/",
                      'swagger_ui_bundle_js': '//unpkg.com/swagger-ui-dist@3'
                                              '/swagger-ui-bundle.js',
                      'swagger_ui_standalone_preset_js': '//unpkg.com'
                                                         '/swagger-ui-dist@3'
                                                         '/swagger-ui'
                                                         '-standalone-preset'
                                                         '.js',
                      'jquery_js': '//unpkg.com/jquery@2.2.4/dist/jquery.min'
                                   '.js',
                      'swagger_ui_css': '//unpkg.com/swagger-ui-dist@3'
                                        '/swagger-ui.css'}
    Swagger(app, config=swagger_config, template=swagger_template)
    # print(Swagger(app, config=swagger_config, template=swagger_template))
