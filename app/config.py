import os
import secrets

def get_env(variable_name):
    if variable_name:
        config_var = os.environ.get(variable_name)
        return config_var
    else:
        raise Exception

hostname = get_env('DB_HOST')
user = get_env('DB_USER')
db_name = get_env('DB_NAME')
password = get_env('DB_PASSWORD')
# secret_key = get_env('SECRET_KEY')
Database_URI = f"mysql+pymysql://root:password@localhost/emp_details"


