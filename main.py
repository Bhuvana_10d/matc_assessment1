from app import create_app
from app.service_node import register_blueprint
app = create_app()


if __name__ == '__main__':
    register_blueprint()
    app.run(debug=True)

